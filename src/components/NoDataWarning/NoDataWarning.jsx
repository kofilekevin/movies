const NoDataWarning = (props) => {
  const { cache } = props;

  return (
    <>
      {!cache ||
        (!cache.length && (
          <div>
            No movies to display. Make sure you're running server.js to fetch
            the list
          </div>
        ))}
    </>
  );
};

export default NoDataWarning;
