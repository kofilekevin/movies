import { render, screen, getByText } from "@testing-library/react";
import NoDataWarning from "./NoDataWarning";

const warningText =
  "No movies to display. Make sure you're running server.js to fetch the list";

test("Should display warning message when cache is empty ", () => {
  render(<NoDataWarning cache={[]} />);
  const warning = screen.getByText(
    /No movies to display. Make sure you're running server.js to fetch the list/i
  );
  expect(warning).toBeInTheDocument();
});
