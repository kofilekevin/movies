import styled from "styled-components";
const SearchBar = (props) => {
  const { updateTextFilterList, cacheList, searchText, setSearchText } = props;

  const filterByText = (text, movies) => {
    if (!movies || !movies.length) {
      throw new Error(
        `Serachbar's filteredByText did not receive an array of movies ${movies}`
      );
    }
    return movies.filter((movie) => movie.title.toLowerCase().includes(text));
  };

  const handleChange = (e) => {
    const text = e.target.value;
    const filteredList = filterByText(text, cacheList);
    setSearchText(text);
    if (searchText.length > 2) {
      updateTextFilterList(filteredList);
    }
    if (searchText.length < 2) {
      updateTextFilterList([]);
    }
  };

  return (
    <SearchInput
      type="text"
      placeholder={"Search"}
      onChange={handleChange}
      value={searchText}
    />
  );
};

const SearchInput = styled.input`
  width: 50%;
  align-self: center;
  margin: 10px 0 10px 0;
`;

export default SearchBar;
