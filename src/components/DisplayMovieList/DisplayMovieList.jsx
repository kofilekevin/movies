import React, { useState } from "react";
import { motion, AnimatePresence } from "framer-motion";

import styled from "styled-components";

const DisplayMovieList = (props) => {
  const { movies, reviews, posterURL } = props;
  const [selectedMovieKey, setSelectedMovieKey] = useState(null);

  const handleTitleClick = (url) => {
    window.location = url;
  };

  const open = (id) => {
    if (id == selectedMovieKey) {
      setSelectedMovieKey(null);
      return;
    }
    setSelectedMovieKey(id);
  };

  const getReview = (id) => {
    let found = reviews.find((review) => review["movie-id"] == id);
    if (found) return found.review;
    if (!found) return "no review - is the api server running?";
  };

  const getPoster = (movie) => {
    const moviePosterURL = movie["cover-url"];
    if (moviePosterURL) {
      const croppedURL = moviePosterURL.split("/api")[1];
      return <PosterImage src={`${posterURL}${croppedURL}`} />;
    }
  };

  return (
    <div>
      {movies &&
        !!movies.length &&
        movies.map((movie, index) => {
          const scoreAsPercent = movie.score * 100;

          return (
            <MovieContainer
              key={movie.id}
              selectedMovieKey={selectedMovieKey}
              onClick={(e) => {
                open(movie.id);
              }}
            >
              <Info
                selectedMovieKey={selectedMovieKey}
                animate={{ x: [-1000, 0] }}
                transition={{ duration: 0.1 * index }}
                exit={{ x: 1000 }}
              >
                {scoreAsPercent}%
                <Link
                  href="#"
                  onClick={(e) => {
                    e.preventDefault();
                    e.stopPropagation();
                    handleTitleClick(movie.url);
                  }}
                >
                  {movie.title}
                </Link>
                ({movie.year})
              </Info>
              <Description
                selectedMovieKey={selectedMovieKey}
                movieId={movie.id}
              >
                {getPoster(movie)}
                {getPoster(movie.id)}
                {getReview(movie.id)}
              </Description>
            </MovieContainer>
          );
        })}
    </div>
  );
};

export default DisplayMovieList;

const PosterImage = styled.img`
  height: 400px;
  padding-bottom: 10px;
`;
const Link = styled.a`
  color: white;
  &:visited {
    color: white;
  }
`;
const MovieContainer = styled(motion.div)`
  display: flex;
  flex-direction: column;
  cursor: pointer;
`;

const Info = styled(motion.div)`
  display: flex;
  justify-content: space-between;
  margin: 10px 0 10px 0;
`;

const Description = styled(motion.div)`
  text-align: center;
  display: ${(props) =>
    props.selectedMovieKey == props.movieId ? "flex" : "none"};
  padding: 10px 0 10px 0;
  flex-direction: column;
  border: solid white 2px;
`;
