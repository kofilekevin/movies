import React, { useState, useEffect } from "react";
import logo from "./logo.svg";
import "./App.css";
import DisplayMovieList from "./components/DisplayMovieList/DisplayMovieList";
import NoDataWarning from "./components/NoDataWarning/NoDataWarning";
import SearchBar from "./components/SearchBar/SearchBar";
import styled from "styled-components";
import space from "./images/space.png";
import {
  generateListOfDecades,
  filterByTitle,
  filterByDecade,
  sortMovies,
  getMovies,
  getReviews,
} from "./utility/utility";

const DecadeDropDownFilter = (props) => {
  const { movies, setDecadeFilteredList, setSelectedDecade } = props;
  const dates = generateListOfDecades(movies);

  const handleSelection = (e) => {
    const selected = e.target.value;
    const filteredMoviesByDate = filterByDecade(selected, movies);
    setDecadeFilteredList(filteredMoviesByDate);
    setSelectedDecade(selected);
    if (!selected) setSelectedDecade(null);
  };

  return (
    <DecadeDropDownContainer>
      <DecadeSelect onChange={handleSelection}>
        <option></option>
        {dates.map((date) => (
          <option key={date}>{date}</option>
        ))}
      </DecadeSelect>
    </DecadeDropDownContainer>
  );
};

const DisplayCache = (props) => {
  const { cache, movies, reviews } = props;

  return (
    <div>
      {!movies ||
        (!movies.length && cache && (
          <DisplayMovieList movies={cache} reviews={reviews} />
        ))}
    </div>
  );
};

function App() {
  const [moviesList, setMoviesList] = useState([]);
  const [textFilteredList, setTextFilteredList] = useState([]);
  const [decadeFilteredList, setDecadeFilteredList] = useState([]);
  const [selectedDecade, setSelectedDecade] = useState(null);
  const [searchText, setSearchText] = useState("");
  const [reviews, setReviews] = useState([]);
  const [cache, setCache] = useState([]);
  const moviesURL = "http://127.0.0.1:1333/api/movies";
  const reviewsURL = "http://127.0.0.1:1333/api/reviews";
  const posterURL = "http://127.0.0.1:1333";

  const checkCached = () => {
    let cached = localStorage.getItem("movies");
    if (cached) {
      setMoviesList(JSON.parse(cached));
      setCache(JSON.parse(cached));
    }
    if (!cached) {
      getMovies(moviesURL).then((movies) => {
        let sortedMovies = sortMovies(movies);
        setMoviesList(sortedMovies);
        setCache(sortedMovies);
        localStorage.setItem("movies", JSON.stringify(sortedMovies));
      });
    }
  };

  const loadReviews = () => {
    getReviews(reviewsURL).then((reviews) => setReviews(reviews));
  };

  const Display = (props) => {
    const { movies, cache } = props;

    return (
      <DisplayContainer>
        <NoDataWarning cache={cache} />
        <DisplayMovieList
          movies={movies}
          reviews={reviews}
          posterURL={posterURL}
        />
        <DisplayCache cache={cache} movies={movies} reviews={reviews} />
      </DisplayContainer>
    );
  };

  useEffect(() => {
    checkCached();
    loadReviews();
  }, []);

  useEffect(() => {
    if (selectedDecade == null || searchText == "") {
      setMoviesList(cache);
    }

    if (selectedDecade) {
      setMoviesList(filterByDecade(selectedDecade, cache));
    }

    if (searchText) {
      setMoviesList(filterByTitle(searchText, cache));
    }

    if (searchText && selectedDecade) {
      let filteredByDecade = filterByDecade(selectedDecade, cache);
      let thenFilteredByTitle = filterByTitle(searchText, filteredByDecade);

      setMoviesList(thenFilteredByTitle);
    }
  }, [textFilteredList, decadeFilteredList]);

  return (
    <AppContainer>
      <Title>Movies Evan Likes!</Title>

      {/* <img src="http://0.0.0.0:1333/assets/arrival-2016.jpg" /> */}
      <DecadeDropDownFilter
        movies={cache}
        setDecadeFilteredList={setDecadeFilteredList}
        setSelectedDecade={setSelectedDecade}
      />
      <SearchBar
        updateTextFilterList={setTextFilteredList}
        cacheList={cache}
        searchText={searchText}
        setSearchText={setSearchText}
      />
      <Labels>
        <div>Rating</div>
        <div>Title</div>
        <div>Year</div>
      </Labels>
      <Display movies={moviesList} cache={cache} />
    </AppContainer>
  );
}

export default App;

const Labels = styled.div`
  display: flex;
  width: 50%;
  justify-content: space-between;
  align-self: center;
  font-weight: bold;
  color: white;
`;
const Title = styled.div`
  font-size: 2em;
  font-weight: bold;
  color: white;
`;

const AppContainer = styled.div`
  display: flex;
  width: 100vw;
  height: 100vh;
  flex-direction: column;
  background: black;
  color: white;
  overflow-y: scroll;
  background-image: url(${space});
  text-align: center;
`;

const DisplayContainer = styled.div`
  width: 50vw;
  align-self: center;
  color: silver;
`;

const DecadeDropDownContainer = styled.div`
  width: 25%;
  align-self: center;
`;

const DecadeSelect = styled.select`
  width: 100%;
`;
