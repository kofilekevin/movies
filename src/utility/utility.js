export const generateListOfDecades = (arrayOfMovieObjects) => {
  const datesRoundedDown = arrayOfMovieObjects.map(
    (movie) => Math.floor(movie.year / 10) * 10
  );
  const removedDuplicates = new Set(datesRoundedDown);
  const setToArray = Array.from(removedDuplicates);
  return setToArray.sort();
};

export const filterByDecade = (decade, movies) => {
  return movies
    .filter((movie) => movie.year >= decade)
    .filter((movie) => movie.year < Number(decade) + 9);
};

export const sortMovies = (unsortedListOfMovieObjects) => {
  const listOfMovieTitles = unsortedListOfMovieObjects.map(
    (movie) => movie.title
  );
  const sortedMovieTitles = listOfMovieTitles.sort();
  const sortedMovieObjects = sortedMovieTitles.map((title) =>
    unsortedListOfMovieObjects.find((movieObj) => movieObj.title == title)
  );
  return sortedMovieObjects;
};

export const filterByTitle = (title, movies) => {
  return movies.filter((movie) => movie.title.toLowerCase().includes(title));
};

export const getMovies = (moviesURL) => {
  return fetch(moviesURL)
    .then((res) => res.json())
    .then((data) => data);
};

export const getReviews = (reviewsURL) => {
  return fetch(reviewsURL)
    .then((res) => res.json())
    .then((data) => data);
};
