import { render, screen, getByText } from "@testing-library/react";

const testMovies = [
  {
    id: 1,
    title: "Arrival",
    year: 1990,
    score: 0.94,
    director: "Denis Villeneuve",
    url: "https://www.rottentomatoes.com/m/arrival_2016",
    synopsis:
      "When mysterious spacecraft touch down across the globe, an elite team--lead by expert linguist Louise Banks (Amy Adams)--are brought together to investigate. As mankind teeters on the verge of global war, Banks and the team race against time for answers--and to find them, she will take a chance that could threaten her life, and quite possibly humanity.",
    rating: "pg-13",
    "runtime-in-minutes": 116,
    "oscar-nominations": 8,
    oscars: 1,
    "cover-url": "/api/assets/arrival-2016.jpg",
  },
  {
    id: 2,
    title: "Coherence",
    year: 2013,
    score: 0.88,
    director: "James Ward Byrkit",
    url: "https://www.rottentomatoes.com/m/coherence_2013",
    synopsis:
      "On the night of an astronomical anomaly, eight friends at a dinner party experience a troubling chain of reality bending events. Part cerebral sci-fi and part relationship drama, COHERENCE is a tightly focused, intimately shot film whose tension intensely ratchets up as its numerous complex mysteries unfold.",
    rating: "nr",
    "runtime-in-minutes": 89,
    "oscar-nominations": 0,
    oscars: 0,
    "cover-url": "/api/assets/coherence-2013.jpg",
  },
  {
    id: 3,
    title: "Interstellar",
    year: 2014,
    score: 0.71,
    director: "Christopher Nolan",
    url: "https://www.rottentomatoes.com/m/interstellar_2014",
    synopsis:
      "With our time on Earth coming to an end, a team of explorers undertakes the most important mission in human history; traveling beyond this galaxy to discover whether mankind has a future among the stars.",
    rating: "pg-13",
    "runtime-in-minutes": 169,
    "oscar-nominations": 5,
    oscars: 1,
    "cover-url": "/api/assets/interstellar-2014.jpg",
  },
];

const sameDecadeItems = [
  {
    id: 3,
    title: "Interstellar",
    year: 2014,
    score: 0.71,
    director: "Christopher Nolan",
    url: "https://www.rottentomatoes.com/m/interstellar_2014",
    synopsis:
      "With our time on Earth coming to an end, a team of explorers undertakes the most important mission in human history; traveling beyond this galaxy to discover whether mankind has a future among the stars.",
    rating: "pg-13",
    "runtime-in-minutes": 169,
    "oscar-nominations": 5,
    oscars: 1,
    "cover-url": "/api/assets/interstellar-2014.jpg",
  },

  {
    id: 2,
    title: "Coherence",
    year: 2013,
    score: 0.88,
    director: "James Ward Byrkit",
    url: "https://www.rottentomatoes.com/m/coherence_2013",
    synopsis:
      "On the night of an astronomical anomaly, eight friends at a dinner party experience a troubling chain of reality bending events. Part cerebral sci-fi and part relationship drama, COHERENCE is a tightly focused, intimately shot film whose tension intensely ratchets up as its numerous complex mysteries unfold.",
    rating: "nr",
    "runtime-in-minutes": 89,
    "oscar-nominations": 0,
    oscars: 0,
    "cover-url": "/api/assets/coherence-2013.jpg",
  },
];

import {
  generateListOfDecades,
  filterByDecade,
  sortMovies,
  filterByTitle,
} from "./utility.js";

test("Should return movies that are from the same decade", () => {
  const sortedList = filterByDecade(2010, testMovies);
  expect(sortedList.length).toEqual(sameDecadeItems.length);
});

test("Should return sorted list of years within source rounded down", () => {
  const listOfDecades = generateListOfDecades(testMovies);
  expect(listOfDecades).toEqual([1990, 2010]);
});
