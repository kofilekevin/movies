const http = require("http");
const path = require("path");
const express = require("express");
const cors = require("cors");
const app = express();
const movies = require("./.impl/api/movies.json");
const reviews = require("./.impl/api/reviews.json");

app.use(express.static(path.join(__dirname, "/api")));

app.get("/api/movies", cors(), function (req, res, next) {
  res.header("Content-Type", "application/json");
  res.json(movies);
});

app.get("/api/reviews", cors(), function (req, res, next) {
  res.header("Content-Type", "application/json");
  res.json(reviews);
});

app.get("/api/posters:id", cors(), function (req, res, next) {
  res.header("Content-Type", "application/json");
  res.json(reviews);
});

app.listen(1333, function () {
  console.log("server started in port 1333");
});
